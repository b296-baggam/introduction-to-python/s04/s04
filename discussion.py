# Python Pillars of OOP

class Bootcamper:
	def __init__(self, name, batch):
		self.name = name
		self.batch = batch

	def info(self):
		print(f"My name is {self.name} of batch {self.batch}")
zuitt_camper = Bootcamper('jeremiah', 100)
print(f'Bootcamper name: {zuitt_camper.name}')
print(f'Bootcamper batch: {zuitt_camper.batch}')
zuitt_camper.info()

# [Section] Encapsulation
# encapsulation is a mechanism of wrapping the attributes and code acting on the methods together as a single unit

# to achieve encapsulation
# declare the attributes of a class
# provide the getters and setters to modify and view the attribute values

class Person():
	def __init__(self):
		# prefix underscore is used as warning for the developers that the property should not be accessed in any way outside its declared class
		self._name = "John Doe"
		# Miniactivity
		self._age = 0

	# getters
	def get_name(self):
		print(f'Name of the person: {self._name}')
	# Miniactivity
	def get_age(self):
		print(f'Age of the person: {self._age}')


	# setters
	def set_name(self, name):
		self._name = name
	# Miniactivity
	def set_age(self, age):
		self._age = age
# why encapsulation
# the fields of a class can be made to read-only and/or write-only
p1 = Person()
p1.get_name()
# print(p1.name)
p1.set_name("Bob Doe")
p1.get_name()
# Miniactivity
# add "age" property to the Person class and add its necessary getters and setters.
# use both getters and setters of the class
# log in the terminal the initial and updated age of p1 instance
# 7:13 pm; kindly send the terminal output in our batch google chat.
p1.get_age()
p1.set_age(28)
p1.get_age()

# [Section] Inheritance
# transfer of characteristics of a parent class to a child class that is derived from it
# class Child_Class(Parent_Class)
class Employee(Person):
	def __init__(self, employee_id):
		# super() is used  to inherit / invoke the immediate parent class constructor
		super().__init__()
		self._employee_id = employee_id
	# methods
	# getters
	def get_employee_id(self):
		print(f'The ID of the employee is {self._employee_id}')
	# setters
	def set_employee_id(self, employee_id):
		self._employee_id = employee_id
	def get_details(self):
		print(f'{self._employee_id} belongs to {self._name}')

emp1 = Employee("001")
emp1.get_details()
emp1.get_employee_id()
emp1.get_name()
emp1.set_name("Jane Doe")
emp1.get_name()
emp1.get_age()
emp1.set_age(26)
emp1.get_age()

# Miniactivity
# 1. create a new class called Student that inherits Person class properties and methods
	# additional attributes: student_no, course, year_level
	# methods: setters and getters of the attributes as well as:
		# get_details: "<student_name> is currently in year <year_level> taking up <course>"
# 7:45 pm: solution discussion; kindly show the terminal outputs in our batch google chat
class Student(Person):
	def __init__(self, student_no, course, year_level):
		super().__init__()
		# attributes
		self._student_no = student_no
		self._course = course
		self._year_level = year_level
	# getters
	def get_student_no(self):
		print(f'Student number is {self._student_no}')
	def get_course(self):
		print(f'Course of the student is {self._course}')
	def get_year_level(self):
		print(f'The student is at year {self._year_level}')
	# setters
	def set_student_no(self, student_no):
		self._student_no = student_no
	def set_course(self, course):
		self._course = course
	def set_year_level(self, year_level):
		self._year_level = year_level
	# details
	def get_details(self):
		print(f'{self._name} is currently in year {self._year_level} taking up {self._course}')
student1 = Student("stdnt-01", "Information Technology", 1)
student1.set_name("Brandon Smith")
student1.set_age(19)
student1.get_details()

# [Section] Polymorphism
# a child class inherits all methods from the parent class. However, in some situations, the method inherited from the parent class doesn't quite fit into the child class. In such cases, we will have to re-implement the method in the child class

# Functions and objects
# a function can be created that can take any object, allowing for polymorphism
class Admin():
	def is_admin(self):
		print(True)
	def user_type(self):
		print('Admin User')

class Customer():
	def is_admin(self):
		print(False)
	def user_type(self):
		print('Regular User')
# define a test function that will take an object called obj
def test_function(obj):
	obj.is_admin()
	obj.user_type()
#  the test_function would call the methods of the object passed to it, hence allowing them to have different outputs, depending on the object passed
user_admin = Admin()
user_reg = Customer()
test_function(user_admin)
test_function(user_reg)

# polymorphism with class methods and loops
# loop calls the methods without being concerned about which class type each object is
class TeamLead():
	def occupation(self):
		print('Team Lead')
	def has_auth(self):
		print(True)
class TeamMember():
	def occupation(self):
		print('Team Member')
	def has_auth(self):
		print(False)
tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	person.occupation()
	person.has_auth()
# the person is assigned as tl1 or tm1 depending on the sequence of the loop

# for user in (user_admin, user_reg):
# 	user.is_admin()
# 	user.user_type()


# polymorphism with inheritance
# polymorphism defined methods in the child class that have the same name as the methods in the parent class
# in inheritance, the child class inherits from the parents. Also, it is possible to modify a method in a child class that was inherited from the parent class
class Zuitt():
	def tracks(self):
		print('We are currently offering 3 tracks: developer career, pi-share career, and short courses')
	def num_of_hours(self):
		print('Learn web development in 360 hours')
class DeveloperCareer(Zuitt):
	# overwrite the parent's method to suit the child's method
	def num_of_hours(self):
		print('Learn web development in 220 hours')

class PiShapeCareer(Zuitt):
	def num_of_hours(self):
		print('Learn web development in 120 hours')

class ShortCourses(Zuitt):
	def num_of_hours(self):
		print('Learn web development in 20 hours')

course1 = DeveloperCareer()
course2 = PiShapeCareer()
course3 = ShortCourses()
print("---")
course1.num_of_hours()
print("---")
course2.num_of_hours()
print("---")
course3.num_of_hours()
print("---")
# for course in (course1, course2, course3):
# 	print("---")
# 	course.num_of_hours()

# [Section] Abstraction
# abstract class can be considered as a blueprint for other classes. it allows us to create a set of method that must created within any child class
# 
# abstract method is a method that has a declaration but does not have an implementation
# the implementation of abstract classes is to be defined in each child class
# by default, python does not have a built-in package for abstract methods/abstract classes. 
# python does come with a module that provides the base for defining Abstract Base Classes (ABC)
from abc import ABC, abstractmethod
# ABC directive creates abstract classes (the class has one or more abstract methods)
class Polygon(ABC):
	# absrtactmethod decorator creates an abstract method that needs to be impplemented byt the child classes that inherit Polygon class
	@abstractmethod
	def print_number_of_sides(self):
		# pass keyword denotes that the method doesn't do anything
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()
	def print_number_of_sides(self):
		print(f'This polygon has 3 sides')

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()
	def print_number_of_sides(self):
		print(f'This polygon has 5 sides')

shape1 = Triangle()
shape1.print_number_of_sides()
shape2 = Pentagon()
shape2.print_number_of_sides()